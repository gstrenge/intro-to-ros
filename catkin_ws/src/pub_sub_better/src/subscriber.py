#!/usr/bin/env python
import rospy
from std_msgs.msg import String

response_prefix = ""

def callback(data):
    rospy.loginfo(f"[{rospy.get_caller_id()}] {response_prefix}{data.data}")
    
def subscriber():


    rospy.init_node('subscriber', anonymous=True)

    global response_prefix
    topic = rospy.get_param("~topic")
    response_prefix = rospy.get_param("~response_prefix")

    rospy.Subscriber(topic, String, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    subscriber()
