#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def publisher():

    rospy.init_node('publisher', anonymous=True)

    topic = rospy.get_param("~topic")
    frequency = rospy.get_param("~frequency")
    message_prefix = rospy.get_param("~message_prefix")

    pub = rospy.Publisher(topic, String, queue_size=10)
    rate = rospy.Rate(frequency) # 10hz
    
    while not rospy.is_shutdown():
        message_to_send = f"{message_prefix}{rospy.get_time()}"
        pub.publish(message_to_send)
        rospy.loginfo(f"Publishing: '{message_to_send}'")
        rate.sleep()

if __name__ == '__main__':
    try:
        publisher()
    except rospy.ROSInterruptException:
        # Gracefully exit
        pass
