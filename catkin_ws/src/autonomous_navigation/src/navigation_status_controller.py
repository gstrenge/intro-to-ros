import rospy
from std_msgs.msg import String, Int8
from random import randint

class NavigationStatus:
    SEARCHING = "Searching"
    SUCCEEDED = "Found Goal"
    FINISHED = "Found Final Goal"


status = NavigationStatus.SEARCHING
start_time = 0
travel_time = randint(3, 15)
wait_time = 4
waypoint_index = 0
waypoint_count = 10

def update_status(current_time):
    global status, start_time, waypoint_index, waypoint_count

    # If we have arrived at the goal
    if status == NavigationStatus.SEARCHING:

        # If we have travelled to the goal we are searching for
        if current_time - start_time > travel_time:
            status = NavigationStatus.SUCCEEDED
            start_time = rospy.get_time()
            
    # If we have found the goal we are looking for
    elif status == NavigationStatus.SUCCEEDED:

        # If we have waited long enough after finding the goal
        if current_time - start_time > wait_time:
            
            # If there are more waypoints
            if waypoint_index + 1 < waypoint_count:
                waypoint_index += 1
                status = NavigationStatus.SEARCHING
            # If we have found all of the waypoints
            else:
                status = NavigationStatus.FINISHED

    # If we are done, do nothing...
    elif status == NavigationStatus.FINISHED:
        pass




def main():

    rospy.init_node("navigation_status_controller", anonymous=True)

    global status, start_time, waypoint_count, waypoint_index

    start_time = rospy.get_time()

    waypoint_pub = rospy.Publisher("current_waypoint_id", Int8, queue_size=10)
    navigation_status_pub = rospy.Publisher("status", String, queue_size=10)

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():

        # Keep track of previous waypoint index so we can check if it updates when we are done
        previous_waypoint_index = waypoint_index

        # Updating state machine
        update_status(rospy.get_time())

        # Publish status at regular rate
        navigation_status_pub.publish(status)
        rospy.loginfo(f"Status: '{status}', Waypoint: {waypoint_index}")

        # If we have updated the waypoint, publish the new one!
        if previous_waypoint_index != waypoint_index:
            waypoint_pub.publish(waypoint_index)

        # Running state machine at specific rate
        rate.sleep()




        

if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass

