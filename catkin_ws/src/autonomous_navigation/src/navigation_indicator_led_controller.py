import rospy
from std_msgs.msg import String, Int8
from random import randint

class NavigationStatus:
    SEARCHING = "Searching"
    SUCCEEDED = "Found Goal"
    FINISHED = "Found Final Goal"

time_of_last_received_status = 0
auto_shutdown_timeout = 1


def status_callback(status_msg):
    status = status_msg.data

    if status == NavigationStatus.SEARCHING:
        

def next_waypoint_callback(waypoint_msg):
    pass

def main():

    global time_of_last_received_status, auto_shutdown_timeout

    rospy.init_node("navigation_indicator_led")

    # Setting initial time to be now
    time_of_last_received_status = rospy.get_time()
    
    rospy.Subscriber("status", String, status_callback)
    rospy.Subscriber("current_waypoint_id", Int8, next_waypoint_callback)

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        if rospy.get_time() - time_of_last_received_status > auto_shutdown_timeout:
            pass

        rate.sleep()




if __name__ == "__main__":
    try:
        main()
    except rospy.ROSInterruptException:
        pass