#include <mbed.h>
#include "platform/mbed_wait_api.h"

int main() {

	DigitalOut searching_indicator(LED1);
	DigitalOut success(LED2);
	DigitalOut finished_indicator(LED4);

	// put your setup code here, to run once:

	while(1) {
		searching_indicator = false;
		success = false;
		finished_indicator = false;

		wait(.5);

		searching_indicator = true;
		success = true;
		finished_indicator = true;

		wait(.5);
	}
}