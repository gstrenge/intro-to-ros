#!/bin/bash

# clone gcc4mbed
pushd $ROVER_WS/tools/firmware > /dev/null

if [ ! -d gcc4mbed ]; then
	git clone https://github.com/adamgreen/gcc4mbed
else
	echo "gcc4mbed Directory already exists."
	exit 1
fi

pushd gcc4mbed > /dev/null

source linux_install

echo "Removing unneeded target header files..."

# Once install is done, get rid of unneeded target header files
find "./external/mbed-os/targets/" -maxdepth 1 -mindepth 1 ! -wholename "*TARGET_STM" -type d -exec rm -rf {} +
find "./external/mbed-os/targets/TARGET_STM" -maxdepth 1 -mindepth 1 ! -wholename "*TARGET_STM32L4" -type d -exec rm -rf {} +
find "./external/mbed-os/targets/TARGET_STM/TARGET_STM32L4" -maxdepth 1 -mindepth 1 ! -wholename "*TARGET_STM32L476xG" ! -wholename "*device*" -type d -exec rm -rf {} +

echo "Done!"

# rm -f {} +
# get back into starting directory
popd > /dev/null
popd > /dev/null
