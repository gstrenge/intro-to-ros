#/!bin/bash

if [ -z $1 ]; then
	echo "Must specify project name"
	exit 1
fi

cd "$ROVER_WS/tools/firmware"

mkdir "$ROVER_WS/firmware/$1" || exit 1
mkdir "$ROVER_WS/firmware/$1/lib"
mkdir "$ROVER_WS/firmware/$1/.vscode"

cp ./project_template/main.cpp "$ROVER_WS/firmware/$1/"
cp ./project_template/.vscode/c_cpp_properties.json "$ROVER_WS/firmware/$1/.vscode/"
cp ./project_template/.vscode/tasks.json "$ROVER_WS/firmware/$1/.vscode/"

export PROJECT_NAME=$1

envsubst '${PROJECT_NAME}' < ./project_template/Makefile > "$ROVER_WS/firmware/$1/Makefile"
envsubst '${PROJECT_NAME}' < ./project_template/.vscode/launch.json > "$ROVER_WS/firmware/$1/.vscode/launch.json"

code "$ROVER_WS/firmware/$1"